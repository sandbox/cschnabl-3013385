<?php

namespace Drupal\email_double_check\Plugin\Field\FieldWidget;

/**
 * Plugin implementation of the 'email_double_check_widget' widget.
 *
 * @FieldWidget(
 *   id = "email_double_check_widget",
 *   label = @Translation("E-Mail Double Check"),
 *   field_types = {
 *     "email",
 *   }
 * )
 */

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;


class EmailDoubleCheckWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'size' => 60,
        'placeholder' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = [
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    ];
    $elements['placeholder'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    $elements['placeholder_email_confirm'] = [
      '#type' => 'textfield',
      '#title' => t('Placeholder Confirm'),
      '#default_value' => $this->getSetting('placeholder_email_confirm'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: @size', ['@size' => $this->getSetting('size')]);
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder')]);
    }
    if (!empty($this->getSetting('placeholder_email_confirm'))) {
      $summary[] = t('Placeholder: @placeholder', ['@placeholder' => $this->getSetting('placeholder_email_confirm')]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $form['#attached']['library'][]= 'email_double_check/email-double-check';
    
    $element['value'] = $element + [
        '#type' => 'textfield',
        '#default_value' => isset($items[$delta]->value) ? $items[$delta]->value : NULL,
        '#size' => $this->getSetting('size'),
        '#placeholder' => $this->getSetting('placeholder'),
        '#maxlength' => $this->getFieldSetting('max_length'),
        '#element_validate' => [
          [static::class, 'validate'],
        ],
      ];

    $element['value_email_confirm'] = [
      '#type' => 'textfield',
      '#title' => t('Confirm Email'),
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder_email_confirm'),
      '#maxlength' => $this->getFieldSetting('max_length'),
      '#required' => TRUE,
      '#element_validate' => [
        [static::class, 'validate'],
      ],


    ];
    return $element;
  }


  /**
   * @param $element
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   */
  public static function validate($element, FormStateInterface $form_state) {
    $value = $element['#value'];

    $value_email_double_check = $form_state->getValue('field_email', '');

    $value_email = $value_email_double_check[0]['value'];
    $value_email_confirm = $value_email_double_check[0]['value_email_confirm'];

    if (strlen($value_email) == 0 || !isset($value_email)) {
      $form_state->setValueForElement($element, '');
      return;
    }

    if ($value_email != $value_email_confirm) {
      $form_state->setError($element, t("Emails must match."));
    }
  }

}